package com.open.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.open.chat.server.config.EnableIMServer;

@SpringBootApplication
@EnableIMServer
//@EnableRocketMqClient
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}