package com.open.chat.server.listener;

import org.jim.core.ImChannelContext;
import org.jim.core.cache.redis.JedisTemplate;
import org.jim.core.exception.ImException;
import org.jim.core.packets.User;
import org.jim.server.ImServerChannelContext;
import org.jim.server.listener.AbstractImUserListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.open.chat.server.common.redis.CommonKey;
import com.open.chat.server.utils.ChannelContextList;
import com.open.chat.server.utils.RedisService;

import cn.hutool.core.util.ObjectUtil;

/**
 * @author WChao
 * @Desc
 * @date 2020-05-02 18:18
 */
@Component
public class ImUserListener extends AbstractImUserListener{
	
    private static Logger logger = LoggerFactory.getLogger(ImUserListener.class);

    @Override
    public void doAfterBind(ImChannelContext imChannelContext, User user) throws ImException {
        logger.info("绑定用户:{}", JSONObject.toJSONString(user));
        //绑定后对通道进行绑定 Token
        //imChannelContext.set("userId", user.getUserId());
        ChannelContextList.channel.put(CommonKey.getUserChannel(user.getUserId()), imChannelContext);
        
    }

    @Override
    public void doAfterUnbind(ImChannelContext imChannelContext, User user) throws ImException {
        logger.info("解绑用户:{}",JSONObject.toJSONString(user));
        ChannelContextList.channel.remove(CommonKey.getUserChannel(user.getUserId()));
    }
}
