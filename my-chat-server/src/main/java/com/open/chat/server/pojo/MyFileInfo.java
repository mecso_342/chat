package com.open.chat.server.pojo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户的文件信息
 * @author admin
 *
 */
@Getter
@Setter
public class MyFileInfo implements Serializable{
	
	private String from ;
	
	private String to;
	/**
	 * 消息类型;(如：0:text、1:image、2:voice、3:vedio)
	 */
	private Integer msgType;
	/**
	 * 聊天类型;(如公聊、私聊)
	 */
	private Integer chatType;
	
}
