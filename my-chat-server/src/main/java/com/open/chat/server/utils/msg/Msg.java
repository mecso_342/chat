package com.open.chat.server.utils.msg;

/**
 * restful规范返回
 * @author
 *
 */
public class Msg implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7645768390334209903L;
	
	/**
	 * 状态码
	 */
	private String code;
	/**
	 * 响应消息
	 */
	private String msg;
	/**
	 * 返回值
	 * 
	 */
	private Object result;

	
	public String getCode() {
		return code;
	}
	public Msg setCode(String code) {
		if(!code.equals(this.code)) {
			if(Status.CODE_OK.equals(code)) {
				msg = "操作成功";
			}else {
				msg = "操作失败";
			}
		}
		this.code = code;
		return this;
	}
	public String getMsg() {
		if(msg == null) {
			if(Status.CODE_OK.equals(this.code)) {
				msg = "操作成功";
			}else {
				msg = "操作失败";
			}
		}
		return msg;
	}
	public Msg setMsg(String msg) {
		this.msg = msg;
		return this;
	}
	@SuppressWarnings("unchecked")
	public <T> T getResult() {
		Object r = result == null ? "": result;
		return (T)r;
	}
	public Msg setResult(Object result) {
		this.result = result;
		return this;
	}
	public static Msg success() {
		return new Msg().setCode(Status.CODE_OK);
	}
	public static Msg success(Object o) {
		return new Msg().setCode(Status.CODE_OK).setResult(o);
	}

	public static Msg success(Object o, String msg) {
		return success(o).setMsg(msg);
	}

	public static Msg error(String code, String msg) {
		return new Msg().setCode(code).setMsg(msg);
	}
	public static Msg error(String msg) {
		return new Msg().setCode(Status.CODE_FAIL).setMsg(msg);
	}
	public static Msg error() {
		return new Msg().setCode(Status.CODE_FAIL);
	}
	public static boolean isOk(Msg Msg) {
		return Status.CODE_OK.equals(Msg.getCode());
	}

	@Override
	public String toString() {
		return "Message{" +
				"code='" + code + '\'' +
				", msg='" + msg + '\'' +
				", result=" + result +
				'}';
	}
	
	
}
