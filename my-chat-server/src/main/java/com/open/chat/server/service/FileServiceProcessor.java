package com.open.chat.server.service;

import org.jim.core.ImChannelContext;
import org.jim.core.ImStatus;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.server.command.handler.extend.pojo.FileInfoReqBody;
import org.jim.server.processor.FileCmdProcessor;
import org.jim.server.protocol.AbstractProtocolCmdProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 上传文件<br/>
 * 上传以后，由nginx代理<br/>
 * TODO 可以以后写一个方法由后端文件流下载,加上鉴权（如：在组内发送的，只有组内的人员才能下载），消耗带宽
 * 目前这个暂时放弃，当文件大了，性能和稳定性不敢保证。更重要的是没有参考的代码,不知道咋写。采用HTTP解决
 * @author admin
 *
 */
@Component
@Deprecated
public class FileServiceProcessor extends AbstractProtocolCmdProcessor implements FileCmdProcessor{
	
	@Value("${app.files.root}")
	private String root;
	@Value("${app.files.basis}")
	private String basis;
	
	private String separator = "/";
	
	@Override
	public RespBody upload(FileInfoReqBody fileInfo, ImChannelContext imChannelContext) {
		RespBody respBody = new RespBody(Command.COMMAND_FILE_RESP,ImStatus.C2001);
		return respBody;
//		ImSessionContext imSessionContext = imChannelContext.getSessionContext();
//		User clientUser = imSessionContext.getImClientNode().getUser();
//		//设置谁发发送的
//		fileInfo.setFrom(clientUser.getUserId());
//		//上传时的原文件名
//		String fileName = fileInfo.getFileName();
//		//拓展名
//		String extentionName = fileName.substring(fileName.lastIndexOf(".")); // 后缀名
//		
//		if(isValid(extentionName, ".gif",".jpg",".jpeg",".bmp",".png")) {
//			fileInfo.setMsgType(MsgType.MSG_TYPE_IMG.getNumber());
//		}else {
//			fileInfo.setMsgType(MsgType.MSG_TYPE_FILE.getNumber());
//		}
//		//处理上传地址  
//		StringBuilder filePath = new StringBuilder();
//		filePath.append(root).append(separator)
//			.append(basis).append(separator)
//			.append(fileInfo.getFrom()).append(separator)
//			.append(IdUtil.simpleUUID()).append(extentionName);
//			
//		String  httpPath =  filePath.substring(root.length());
//		//.writeByteArrayToFile(file, uploadFile.getData());
//		
//		File f = FileUtil.writeBytes(fileInfo.getFileData(), filePath.toString());
//		MyFileInfo my = new MyFileInfo();
//		if(f.exists()) {
//			my.setFrom(fileInfo.getFrom());
//			my.setTo(fileInfo.getTo());
//			my.setCreateTime(DateUtil.current());
//			my.setChatType(fileInfo.getChatType());
//			my.setMsgType(fileInfo.getMsgType());
//			my.setFileName(fileName);
//			my.setFileUrl(httpPath);
//		}else {
//			respBody.setCode(ImStatus.C2001.getCode()).setMsg("上传失败");
//		}
//		return respBody.setData(my);
	}
	
	public static boolean isValid(String type, String... allowTypes) {
		if (null == type || "".equals(type.trim())) {
			return false;
		}
		for (String t : allowTypes) {
			if (type.equals(t)) {
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args) {
		String name = "d/root/upload/a.jpg";
		StringBuilder filePath = new StringBuilder(name);
		String  httpPath =  filePath.substring("d/root".length());
		System.out.println(httpPath);
	}
}
