package com.open.chat.server.common.redis;

import org.jim.core.ImConst;

/**
 * 缓存数据库Key
 * @author admin
 *
 */
public interface  CommonKey {
	
	static  String SUFFIX = ":";
	/**
	 * 获取用户的相关key
	 * @return
	 */
	public  static String getUserInfo(String userId) {
		
		return userId+SUFFIX+ImConst.INFO;
	}
	
	/**
	 * 获取用户的所有组
	 * @return
	 */
	public  static String getUserGroup(String userId) {
		
		return userId+SUFFIX+ImConst.GROUP;
	}
	
	/**
	 * 获取用户的协议的在线状态
	 * terminal :ws 、android
	 * @return
	 */
	public  static String getTerminal(String userId,String terminal) {
		return userId+SUFFIX+ImConst.TERMINAL+SUFFIX+terminal;
	}
	
	

	/**
	 * 获取组的相关信息 
	 * @return
	 */
	public  static String getGroupInfo(String groupId) {
	
		return groupId+SUFFIX+ImConst.INFO;
	}
	/**
	 * 获取组的用户列表
	 * @return
	 */
	public  static String getGroupUsers(String groupId) {
		
		return groupId+SUFFIX+ImConst.USER;
	}
	
	
	/**
	 * 鉴权成功后，绑定userId 和 通道,需要存入redis
	 * 用在上次文件上
	 * @return
	 */
	public  static String getUserChannel(String  userId ) {
		
		return "channel"+ SUFFIX + userId;
	}
	
}
