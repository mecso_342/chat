package com.open.chat.server.common;

public interface Common {
	
	
	String COMMA = ",";
	String SEPARATOR = "/";
	/**
	 * 系统账户userId
	 */
	String SYS_USER_ID = "s100";
	
	String SYS_USER_NAME = "系统账户";
	
	/**
	 * 存入组的序列KEY
	 */
	String GROUP_ID_KEY = "GROUP_ID_KEY";
	/**
	 * 存入组的序列起始值
	 */
	Integer GROUP_ID_CODE_START = 30000;
	
	public interface UserInfoExtKeys{
		String id = "id";
		String sex = "sex";
		String email = "email";
		String department = "department";
		String position = "position";
		String policeType = "policeType";
		
	}
	
	
	
	
	
}
