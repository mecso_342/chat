package com.open.chat.server.utils;

import java.util.HashMap;
import java.util.Map;

import org.jim.core.ImChannelContext;

/**
 * 存储用户登录的通道列表
 * @author admin
 *
 */
public class ChannelContextList {
	
	/**
	 * 通道列表<br/>
	 * TODO 集群时，需要改这部分东西。很明显它对集群不支持。
	 */
	public static Map<String, ImChannelContext> channel = new HashMap<>();
	
}
