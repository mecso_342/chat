package com.open.chat.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EnableIMConfiguration{
	
	@Autowired
	private  ImServerStarter stater;
	
	
	@Bean
	public void clientStart() throws Exception {
		stater.run();
	}

}
