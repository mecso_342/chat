package com.open.chat.server.common;

public interface MsgCode {
    /** [GET]：服务器成功返回用户请求的数据，如果查询不到数据为404**/
     Integer CODE_OK = 200;

    /** [GET]：用户处理的失败请求，如：删除、新增、修改，成功为200**/
    Integer CODE_FAIL = 201;

    /**  表示用户没有权限（令牌、用户名、密码错误） (未经验证的用户，常见于未登录)**/
    Integer CODE_UNAUTHORIZED = 401;

    /**  无权限，表示用户得到授权（与401错误相对），但是访问是被禁止的(如果经过验证后依然没权限，应该 403) **/
    Integer CODE_FORBIDDEN = 403;

    Integer CODE_ERROR = 500;
}
