package com.open.chat.server.pojo;

import org.jim.core.packets.Message;
import org.jim.core.packets.RespBody;

import com.alibaba.fastjson.JSONObject;

public class SystemNotifyRespBody extends Message{

	/**
	 *
	 */
	private static final long serialVersionUID = 5463401717899424178L;
	
	
	
	/**
	 * 发给谁的
	 */
	private String toUserId;
	private String toUserName;
	/**
	 * 发给谁的
	 */
	private String toGroupId;
	private String toGroupName;
	/**
	 * 是否需要操作  0:无   1:创建群 2:加群  3:退群  4：解散群
	 */
	private Integer oper;
	
	/**
	 * 时间
	 */
	private Long createTime;
	
	private String data;
	/**
	 * 拓展参数
	 */
	private  JSONObject extras;
	
	

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getToGroupId() {
		return toGroupId;
	}

	public void setToGroupId(String toGroupId) {
		this.toGroupId = toGroupId;
	}

	public Integer getOper() {
		return oper;
	}

	public void setOper(Integer oper) {
		this.oper = oper;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getToGroupName() {
		return toGroupName;
	}

	public void setToGroupName(String toGroupName) {
		this.toGroupName = toGroupName;
	}

	public JSONObject getExtras() {
		return extras;
	}

	public void setExtras(JSONObject extras) {
		this.extras = extras;
	}

	

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	
	
	

}
