package com.open.chat.server.utils.msg;


/**

@description 消息类输出Code值
@author 
@date 2017年8月14日 下午3:03:05

*/
public interface Status {
	/** [GET]：服务器成功返回用户请求的数据，如果查询不到数据为404**/
	String CODE_OK = "200";
	
	/** [GET]：用户处理的失败请求，如：删除、新增、修改，成功为200**/
	String CODE_FAIL = "201";

	/**  表示用户没有权限（令牌、用户名、密码错误） (未经验证的用户，常见于未登录)**/
	String CODE_UNAUTHORIZED = "401";
	
	/**  无权限，表示用户得到授权（与401错误相对），但是访问是被禁止的(如果经过验证后依然没权限，应该 403) **/
	String CODE_FORBIDDEN = "403";
	/**
	 * 请求入参错误
	 */
	String CODE_PARA_ERR = "202";
}
