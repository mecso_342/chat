package com.open.chat.server.config;

import org.apache.commons.lang3.StringUtils;
import org.jim.core.packets.Command;
import org.jim.core.utils.PropUtil;
import org.jim.server.JimServer;
import org.jim.server.command.CommandManager;
import org.jim.server.command.handler.AuthReqHandler;
import org.jim.server.command.handler.ChatReqHandler;
import org.jim.server.command.handler.HandshakeReqHandler;
import org.jim.server.command.handler.extend.AddGroupReqHandler;
import org.jim.server.command.handler.extend.AddUserToGroupReqHandler;
import org.jim.server.command.handler.extend.EditGroupReqHandler;
import org.jim.server.command.handler.extend.ExitGroupReqHandler;
import org.jim.server.command.handler.extend.GroupInfoReqHandler;
import org.jim.server.command.handler.extend.ReCallReqHandler;
import org.jim.server.command.handler.extend.RemoveGroupReqHandler;
import org.jim.server.command.handler.extend.SystemNotifyReqHandler;
import org.jim.server.config.ImServerConfig;
import org.jim.server.config.PropertyImServerConfigBuilder;
import org.jim.server.processor.chat.DefaultAsyncChatMessageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tio.core.ssl.SslConfig;

import com.open.chat.server.command.TokenWsHandshakeProcessor;
import com.open.chat.server.listener.ImGroupListener;
import com.open.chat.server.listener.ImUserListener;
import com.open.chat.server.service.AuthServiceProcessor;
import com.open.chat.server.service.FileServiceProcessor;
import com.open.chat.server.service.GroupServiceProcessor;
import com.open.chat.server.service.ReCallMessageServiceProcessor;
import com.open.chat.server.service.SystemNotifyServiceProcessor;

@Component
public class ImServerStarter {
	
	@Autowired
	private AuthServiceProcessor authServiceProcessor;
	
	@Autowired
	private  FileServiceProcessor fileServiceProcessor;
	@Value("${app.chat.path}")
	private  String jim;
	@Autowired
	private ImUserListener imUserListener;
	public void run()  throws Exception{
		
		//ImServerConfig imServerConfig = new PropertyImServerConfigBuilder("config/jim.properties").build();
		ImServerConfig imServerConfig = new PropertyImServerConfigBuilder(jim).build();
		//初始化SSL;(开启SSL之前,你要保证你有SSL证书哦...)
		//initSsl(imServerConfig);
		//设置群组监听器，非必须，根据需要自己选择性实现;
		imServerConfig.setImGroupListener(new ImGroupListener());
		//设置绑定用户监听器，非必须，根据需要自己选择性实现;
		imServerConfig.setImUserListener(imUserListener);
		JimServer jimServer = new JimServer(imServerConfig);
		
		//握手
		HandshakeReqHandler handshakeReqHandler = CommandManager.getCommand(Command.COMMAND_HANDSHAKE_REQ, HandshakeReqHandler.class);
		//添加自定义握手处理器; AuthReqHandler
		handshakeReqHandler.addMultiProtocolProcessor(new TokenWsHandshakeProcessor());
		
		//LoginReqHandler loginReqHandler = CommandManager.getCommand(Command.COMMAND_LOGIN_REQ,LoginReqHandler.class);
		//添加登录业务处理器;
		//loginReqHandler.setSingleProcessor(new LoginServiceProcessor());
		
		AuthReqHandler  authReqHandler  = CommandManager.getCommand(Command.COMMAND_AUTH_REQ,AuthReqHandler.class);
		authReqHandler.setSingleProcessor(authServiceProcessor);
		
		
		//添加用户业务聊天记录处理器，用户自己继承抽象类BaseAsyncChatMessageProcessor即可，以下为内置默认的处理器！
		ChatReqHandler chatReqHandler = CommandManager.getCommand(Command.COMMAND_CHAT_REQ, ChatReqHandler.class);
		chatReqHandler.setSingleProcessor(new DefaultAsyncChatMessageProcessor());
		

		//组业务处理
		AddGroupReqHandler   addGroupReqHandler  = CommandManager.getCommand(Command.COMMAND_ADD_GROUP_REQ, AddGroupReqHandler.class);
		addGroupReqHandler.setSingleProcessor(new GroupServiceProcessor());	
		
		EditGroupReqHandler   editGroupReqHandler  = CommandManager.getCommand(Command.COMMAND_EDIT_GROUP_REQ, EditGroupReqHandler.class);
		editGroupReqHandler.setSingleProcessor(new GroupServiceProcessor());	
		
		AddUserToGroupReqHandler   addUserToGroupReqHandler  = CommandManager.getCommand(Command.COMMAND_ADD_USER_TO_GROUP_REQ, AddUserToGroupReqHandler.class);
		addUserToGroupReqHandler.setSingleProcessor(new GroupServiceProcessor());	
		
		RemoveGroupReqHandler   removeGroupReqHandler  = CommandManager.getCommand(Command.COMMAND_REMOVE_GROUP_REQ, RemoveGroupReqHandler.class);
		removeGroupReqHandler.setSingleProcessor(new GroupServiceProcessor());	
		
			
		ExitGroupReqHandler   exitGroupReqHandler  = CommandManager.getCommand(Command.COMMAND_EXIT_GROUP_REQ, ExitGroupReqHandler.class);
		exitGroupReqHandler.setSingleProcessor(new GroupServiceProcessor());	
		
		GroupInfoReqHandler   groupInfoReqHandler  = CommandManager.getCommand(Command.COMMAND_INFO_GROUP_REQ, GroupInfoReqHandler.class);
		groupInfoReqHandler.setSingleProcessor(new GroupServiceProcessor());
		
		//系统通知
		SystemNotifyReqHandler   systemNotifyReqHandler = CommandManager.getCommand(Command.COMMAND_SYS_NOTIFY_REQ, SystemNotifyReqHandler.class);
		//fileReqHandler.setSingleProcessor(new FileServiceProcessor());	
		systemNotifyReqHandler.setSingleProcessor(new SystemNotifyServiceProcessor());
		
		//撤回消息
		ReCallReqHandler   reCallReqHandler = CommandManager.getCommand(Command.COMMAND_RECAL_REQ, ReCallReqHandler.class);
		reCallReqHandler.setSingleProcessor(new ReCallMessageServiceProcessor());
		/*****************end *******************************************************************************************/
		jimServer.start();
		
		
		
	}
	
	/**
	 * 开启SSL之前，你要保证你有SSL证书哦！
	 * @param imServerConfig
	 * @throws Exception
	 */
	public void initSsl(ImServerConfig imServerConfig) throws Exception {
		//开启SSL
		if(ImServerConfig.ON.equals(imServerConfig.getIsSSL())){
			String keyStorePath = PropUtil.get("jim.key.store.path");
			String keyStoreFile = keyStorePath;
			String trustStoreFile = keyStorePath;
			String keyStorePwd = PropUtil.get("jim.key.store.pwd");
			if (StringUtils.isNotBlank(keyStoreFile) && StringUtils.isNotBlank(trustStoreFile)) {
				SslConfig sslConfig = SslConfig.forServer(keyStoreFile, trustStoreFile, keyStorePwd);
				imServerConfig.setSslConfig(sslConfig);
			}
		}
	}
	
}
