package com.open.chat.server.service;

import java.util.ArrayList;
import java.util.List;

import org.jim.core.ImChannelContext;
import org.jim.core.ImConst;
import org.jim.core.ImStatus;
import org.jim.core.cache.redis.RedisCacheManager;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.core.packets.SystemNotify;
import org.jim.core.utils.JsonKit;
import org.jim.server.command.handler.extend.pojo.SystemNotifyRespBody;
import org.jim.server.processor.SystemNotifyCmdProcessor;
import org.jim.server.protocol.AbstractProtocolCmdProcessor;

import com.open.chat.server.common.redis.CommonKey;

public class SystemNotifyServiceProcessor  extends AbstractProtocolCmdProcessor implements SystemNotifyCmdProcessor{
	
	private static Integer pageSize = 10;
	
	@Override
	public RespBody list(SystemNotify systemNotify, ImChannelContext imChannelContext) {
		RespBody respBody = new RespBody(Command.COMMAND_SYS_NOTIFY_RESP,ImStatus.C2001);
		//List<String> list = RedisCacheManager.getCache(ImConst.USER).sortSetGetAll(systemNotify.getUserId() + CommonKey.SUFFIX + ImConst.SYS_NOTIFY);
		List<String> list = new ArrayList<String>();
		if(systemNotify.getPageCount() == 0) {
			list = RedisCacheManager.getCache(ImConst.USER)
					.sortSetReverseGetAll(systemNotify.getUserId() + CommonKey.SUFFIX + ImConst.SYS_NOTIFY,0,Double.MAX_VALUE);
		}else {
			//
			list = RedisCacheManager.getCache(ImConst.USER)
				.sortSetReverseGetAll(systemNotify.getUserId() + CommonKey.SUFFIX + ImConst.SYS_NOTIFY,
							0,Double.MAX_VALUE,getOffset(systemNotify.getPageCount()),pageSize);
		}
		List<SystemNotifyRespBody> messageDataList = JsonKit.toArray(list, SystemNotifyRespBody.class);
		return respBody.setData(messageDataList);
	}
	
	
	 public Integer getOffset(int pageNumber) {
	        if (pageNumber <= 1) {
	            pageNumber = 1;
	        }
	        
	        return (pageNumber-1)*pageSize;
	    }

}
