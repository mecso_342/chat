/**
 * 
 */
package org.jim.core.packets;

import com.alibaba.fastjson.JSONObject;

/**
 * 聊天记录
 * @author admin
 *
 */
public class ChatRecordBody extends Message {
	
	private static final long serialVersionUID = 5731474214655476286L;
	/**
	 *  如果Type为1 ，则ID为userId；如果Type为2，则Id为groupId
	 */
	private String id;
	/**
	 * （1:个人  2：组）
	 */
	private Integer type;
	
	

	private ChatRecordBody(){}
	
	private ChatRecordBody(String id , String from , String to , Integer msgType , Integer chatType , String content , String groupId , Integer cmd , Long createTime , JSONObject extras){
		this.id = id;
		this.cmd = cmd;
		this.createTime = createTime;
		this.extras = extras;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
}
