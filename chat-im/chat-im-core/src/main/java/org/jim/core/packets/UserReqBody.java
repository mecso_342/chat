/**
 * 
 */
package org.jim.core.packets;

/**
 * 版本: [1.0]
 * 功能说明: 
 * 作者: WChao 创建时间: 2017年9月26日 上午11:44:10
 */
public class UserReqBody extends Message{
	
	private static final long serialVersionUID = 1861307516710578262L;
	/**
	 * 用户id;
	 */
	private String userId;
	/**
	 * 0:单个,1:所有在线用户,2:所有用户(在线+离线);
	 */
	private Integer type;
	/**
	 * 好友分组id;
	 */
	private String friendGroupId;
	/**
	 * 群组id;
	 */
	private String groupId;
	
	/**
	 * 是否仅仅是用户信息 true 仅仅是用户信息  false包含组信息
	 */
	private Boolean onlyUserStatus;
	
	/**
	 * 同一个消息，前端不同业务处理
	 */
	protected Integer oper ;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getFriendGroupId() {
		return friendGroupId;
	}

	public void setFriendGroupId(String friendGroupId) {
		this.friendGroupId = friendGroupId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Integer getOper() {
		return oper;
	}

	public void setOper(Integer oper) {
		this.oper = oper;
	}

	public Boolean getOnlyUserStatus() {
		return onlyUserStatus;
	}

	public void setOnlyUserStatus(Boolean onlyUserStatus) {
		this.onlyUserStatus = onlyUserStatus;
	}
}
