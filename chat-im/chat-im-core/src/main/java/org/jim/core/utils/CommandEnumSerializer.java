package org.jim.core.utils;

import java.io.IOException;
import java.lang.reflect.Type;

import org.jim.core.packets.Command;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;

public class CommandEnumSerializer implements ObjectSerializer{

	@Override
	public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features)
			throws IOException {
		
		Command   c = (Command)object;
		serializer.out.writeInt(c.getNumber());
	}

}
