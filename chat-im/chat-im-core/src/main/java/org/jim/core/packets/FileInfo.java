package org.jim.core.packets;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户的文件信息
 * @author admin
 *
 */
@Getter
@Setter
public class FileInfo implements java.io.Serializable{
	
	private String fileName;
	
	private String fileUrl;
	
	private Long fileSize;
	
	
}
