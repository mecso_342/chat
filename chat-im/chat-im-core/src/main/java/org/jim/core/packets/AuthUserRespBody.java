package org.jim.core.packets;

import org.jim.core.Status;

public class AuthUserRespBody extends RespBody{
	
	public AuthUserRespBody(Command command , Status status) {
		super(command, status);
	}
	public AuthUserRespBody(Command command,Object data) {
		super(command, data);
	}
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
