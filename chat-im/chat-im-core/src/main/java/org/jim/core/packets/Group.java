/**
 * 
 */
package org.jim.core.packets;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import cn.hutool.core.bean.BeanUtil;

/**
 * 版本: [1.0]
 * 功能说明: 用户群组
 * 作者: WChao 创建时间: 2017年9月21日 下午1:54:04
 */
public class Group extends Message{
	
	private static final long serialVersionUID = -3817755433171220952L;
	/**
	 * 群组ID
	 */
	private String groupId;
	/**
	 * 群组名称
	 */
	private String name;
	/**
	 * 群组头像
	 */
	private String avatar;
	/**
	 * 在线人数
	 */
	private Integer online;
	/**
	 * 群主用户id
	 */
	private String leaderId;
	
	/**
	 * 组用户
	 */
	private List<User> users;
	
	/**
	 * 组介绍
	 */
	private String content;
	
	/**
	 * 组更新时间
	 */
	private Long updTime;
	
	

	public Long getUpdTime() {
		return updTime;
	}

	public void setUpdTime(Long updTime) {
		this.updTime = updTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	

	private Group(){}

	private Group(String groupId , String name, String avatar, 
				Integer online,String leaderId,String content ,Long updTime,
				List<User> users, JSONObject extras){
		this.groupId = groupId;
		this.name = name;
		this.avatar = avatar;
		this.online = online;
		this.users = users;
		this.extras = extras;
		this.leaderId=leaderId;
		this.content = content;
		this.updTime = updTime;
	}

	public static Group.Builder newBuilder(){
		return new Group.Builder();
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getOnline() {
		return online;
	}

	public void setOnline(Integer online) {
		this.online = online;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public String getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(String leaderId) {
		this.leaderId = leaderId;
	}

	public static class Builder extends Message.Builder<Group, Group.Builder>{
		/**
		 * 群组ID
		 */
		private String groupId;
		/**
		 * 群组名称
		 */
		private String name;
		/**
		 * 群组头像
		 */
		private String avatar;
		/**
		 * 在线人数
		 */
		private Integer online;
		/**
		 * 组用户
		 */
		private List<User> users = null;
		/**
		 * 群主，用户id
		 */
		private String leaderId;
		
		/**
		 * 组介绍
		 */
		private String content;
		
		/**
		 * 组添加时间
		 */
		private Long updTime;
		
		public Builder(){};

		public Group.Builder groupId(String groupId) {
			this.groupId = groupId;
			return this;
		}
		public Group.Builder name(String name) {
			this.name = name;
			return this;
		}
		public Group.Builder avatar(String avatar) {
			this.avatar = avatar;
			return this;
		}
		public Group.Builder setChatType(Integer online) {
			this.online = online;
			return this;
		}
		public Group.Builder setLeaderId(String leaderId) {
			this.leaderId = leaderId;
			return this;
		}
		public Group.Builder content(String content) {
			this.content = content;
			return this;
		}
		public Group.Builder updTime(Long updTime) {
			this.updTime = updTime;
			return this;
		}
		public Group.Builder addUser(User user) {
			if(CollectionUtils.isEmpty(users)){
				users = Lists.newArrayList();
			}
			users.add(user);
			return this;
		}
		@Override
		protected Group.Builder getThis() {
			return this;
		}

		@Override
		public Group build(){
			return new Group(this.groupId , this.name , this.avatar , 
					this.online ,this.leaderId, this.content ,this.updTime,
					this.users, this.extras);
		}
	}

	public Group clone(){
		Group group = Group.newBuilder().build();
		BeanUtil.copyProperties(this, group,"users");
		return group;
	}

}
