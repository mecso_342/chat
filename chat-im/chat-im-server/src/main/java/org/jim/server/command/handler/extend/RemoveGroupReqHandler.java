package org.jim.server.command.handler.extend;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.ImStatus;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.Group;
import org.jim.core.packets.RespBody;
import org.jim.core.utils.JsonKit;
import org.jim.server.JimServerAPI;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.processor.group.GroupCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 解散群
 * @author admin
 *
 */
public class RemoveGroupReqHandler extends AbstractCmdHandler {
	
	private static Logger log = LoggerFactory.getLogger(RemoveGroupReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_REMOVE_GROUP_RESP,ImStatus.C2002);
		//组信息
		Group group = JsonKit.toBean(packet.getBody(),Group.class);
		String groupId = group.getGroupId();
		if (StringUtils.isBlank(groupId)) {
			log.error("group is null,{}", imChannelContext);
			respBody.setMsg("群组不存在");
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			return respPacket;
		}
		//如果有额外业务
		GroupCmdProcessor groupProcessor = this.getSingleProcessor(GroupCmdProcessor.class);
		if(Objects.nonNull(groupProcessor)){
			respBody = groupProcessor.unbind(group.getGroupId(), imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (status) {
				respBody  = new RespBody(Command.COMMAND_REMOVE_GROUP_RESP, ImStatus.C2002);
			}
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			//respPacket.setStatus(ImStatus.C2002);
			return respPacket;
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_REMOVE_GROUP_REQ;
	}
}
