package org.jim.server.command.handler.extend.utils;

import org.jim.core.ImConst;
import org.jim.core.ImPacket;
import org.jim.core.cache.redis.RedisCacheManager;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.server.JimServerAPI;
import org.jim.server.command.handler.extend.pojo.SystemNotifyRespBody;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.core.date.DateUtil;


/**
 * 系统通知
 * @author admin
 *
 */
public class SystemNotify {
	
	/**
	 * 操作类型
	 * @author admin
	 *
	 */
	public interface Operation{
		/***无**/
		Integer oper_no = 0;
		/***创建群**/
		Integer oper_crt_group = 1;
		/***加入群**/
		Integer oper_join_group = 2;
		/***退出群**/
		Integer oper_exit_group = 3;
		/***删除群**/
		Integer oper_remove_group = 4;
		/***修改群**/
		Integer oper_edit_group = 5;
		/***撤回消息**/
		Integer oper_recall_msg = 6;
		/***聊天列表业务删除**/
		Integer oper_chat_list_remove = 7;
	}
	/**
	 * 通知到组
	 */
	public static void notifyGroup(String groupId,String groupName,String content) {
		
		notifyGroup(groupId, groupName, content,Operation.oper_no, null);
		
	}
	/**
	 * 通知到组
	 */
	public static void notifyGroup(String groupId,String groupName,String content,Integer oper) {
		
		notifyGroup(groupId, groupName, content,oper, null);
		
	}
	/**
	 * 通知到组
	 */
	public static void notifyGroup(String groupId,String groupName,
						String content,Integer oper,
						JSONObject extras) {
		
		SystemNotifyRespBody body = new SystemNotifyRespBody();
		
		//body.setCmd(Command.SYSTEM_RESP.getNumber());
		body.setToGroupId(groupId);
		body.setToGroupName(groupName);
		body.setData(content);
		body.setCreateTime(DateUtil.current());
		body.setOper(oper);
		if(extras != null) {
			body.setExtras(extras);
		}
		RespBody resp = new RespBody(Command.SYSTEM_RESP, body);
		ImPacket imPacket = new ImPacket(Command.SYSTEM_RESP, resp.toByte());
		JimServerAPI.sendToGroup(groupId, imPacket);
		
	}
	
	public static void notifyUser(String userId,String userName,String content) {
		notifyUser(userId, userName, content,Operation.oper_no, true,null);
	}
	public static void notifyUser(String userId,String userName,String content,Integer oper) {
		if(Operation.oper_no == oper) {
			notifyUser(userId, userName, content,oper, true,null);
		}else {
			notifyUser(userId, userName, content,oper, false,null);
		}
		
	}
	public static void notifyUser(String userId,String userName,String content,Integer oper,JSONObject extras) {
		if(Operation.oper_no == oper) {
			notifyUser(userId, userName, content,oper, true,extras);
		}else {
			notifyUser(userId, userName, content,oper, false,extras);
		}
		
	}
//	public static void notifyUser(String userId,String userName,String content,boolean cache) {
//		notifyUser(userId, userName, content,Operation.oper_no,cache, null);
//	}
	/**
	 * 通知到人
	 */
	public static void notifyUser(String userId,String userName,
						String content,Integer oper,boolean cache,JSONObject extras) {
		SystemNotifyRespBody body = new SystemNotifyRespBody();
		//body.setCmd(Command.SYSTEM_RESP.getNumber());
		body.setToUserId(userId);
		body.setToUserName(userName);
		body.setData(content);
		body.setCreateTime(DateUtil.current());
		body.setOper(oper);
		if(extras != null) {
			body.setExtras(extras);
		}
		RespBody resp = new RespBody(Command.SYSTEM_RESP, body);
		ImPacket imPacket = new ImPacket(Command.SYSTEM_RESP,resp.toByte());
		//入缓存
		if(cache) {
			//writeMessage(PUSH,USER+":"+to+":"+from,chatBody);
			writeMessage(ImConst.USER,userId + ":" + ImConst.SYS_NOTIFY,body);
			
		}
		JimServerAPI.sendToUser(userId, imPacket);
	}
	
	public static void writeMessage(String timelineTable , String timelineId , SystemNotifyRespBody body){
		RedisCacheManager.getCache(timelineTable).sortSetPush(timelineId, body.getCreateTime(), body);
	}

}
