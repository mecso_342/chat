package org.jim.server.command.handler.extend;

import org.apache.commons.lang3.StringUtils;
import org.jim.core.*;
import org.jim.core.exception.ImException;
import org.jim.server.JimServerAPI;
import org.jim.server.processor.group.GroupCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jim.core.packets.Command;
import org.jim.core.packets.Group;
import org.jim.core.packets.JoinGroupRespBody;
import org.jim.core.packets.JoinGroupResult;
import org.jim.core.packets.RespBody;
import org.jim.core.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.extend.pojo.UserOfGroup;

import java.util.Objects;

/**
 * 
 *添加用户到组
 *
 */
public class AddUserToGroupReqHandler extends AbstractCmdHandler {
	
	private static Logger log = LoggerFactory.getLogger(AddUserToGroupReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_ADD_USER_TO_GROUP_RESP,ImStatus.C2002);
		UserOfGroup group = JsonKit.toBean(packet.getBody(),UserOfGroup.class);
		if (StringUtils.isBlank(group.getGroupId())) {
			log.error("group is null,{}", imChannelContext);
			respBody.setMsg("群组不存在");
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			return respPacket;
		}
		if (StringUtils.isBlank(group.getUserIds())) {
			log.error("userId  is null,{}", imChannelContext);
			respBody.setMsg("邀请的用户不存在");
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			return respPacket;
		}
		//如果有额外业务
		GroupCmdProcessor groupProcessor = this.getSingleProcessor(GroupCmdProcessor.class);
		if(Objects.nonNull(groupProcessor)){
			respBody = groupProcessor.addUserToGroup(group.getGroupId(),group.getUserIds(), imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (status) {
				respBody  = new RespBody(Command.COMMAND_ADD_USER_TO_GROUP_RESP, ImStatus.C2002);
			}
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			//respPacket.setStatus(ImStatus.C2002);
			return respPacket;
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_ADD_USER_TO_GROUP_REQ;
	}
}
