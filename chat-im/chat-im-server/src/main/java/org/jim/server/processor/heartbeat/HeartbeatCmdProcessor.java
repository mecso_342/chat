package org.jim.server.processor.heartbeat;

import org.jim.core.ImChannelContext;

/**
 * 心跳功能拓展
 * @author admin
 *
 */
public interface HeartbeatCmdProcessor{

	void close(String userId,ImChannelContext imChannelContext);
}
