package org.jim.server.command.handler.extend.pojo;

import org.jim.core.packets.Group;
import org.jim.core.packets.Message;
import org.jim.core.packets.User;

/**
 * 用户所在的组
 * @author admin
 *
 */
public class UserOfGroup extends Message{

	/**
	 *
	 */
	private static final long serialVersionUID = 1847053515209041611L;

	/**
	 * 申请进入组或移除组的用户ID
	 */
	private String userIds;
	
	/**
	 * 该用户申请进入或移除的组
	 */
	private String groupId;
	
	/**
	 * 1：退群   2：踢出
	 */
	private Integer type;
	

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


	
	
}
