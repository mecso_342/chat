package org.jim.server.processor;

import org.jim.core.ImChannelContext;
import org.jim.core.packets.RespBody;
import org.jim.server.command.handler.extend.pojo.FileInfoReqBody;

public interface FileCmdProcessor extends  SingleProtocolCmdProcessor{
	
	
	 RespBody upload(FileInfoReqBody fileInfo, ImChannelContext imChannelContext);
	
}
