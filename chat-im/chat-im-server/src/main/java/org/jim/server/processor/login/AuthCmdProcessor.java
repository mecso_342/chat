/**
 * 
 */
package org.jim.server.processor.login;

import org.jim.core.ImChannelContext;
import org.jim.core.packets.AuthReqBody;
import org.jim.core.packets.AuthUserRespBody;
import org.jim.core.packets.User;
import org.jim.core.packets.User;
import org.jim.server.processor.SingleProtocolCmdProcessor;
/**
 *
 * @author WChao
 */
public interface AuthCmdProcessor extends SingleProtocolCmdProcessor {
	/**
	 * 执行登录操作接口方法
	 * @param loginReqBody
	 * @param imChannelContext
	 * @return
	 */
	AuthUserRespBody doAuth(AuthReqBody authReqBody, ImChannelContext imChannelContext);
	
	/**
	 * 登录成功(指的是J-IM会在用户校验完登陆逻辑后进行J-IM内部绑定)回调方法
	 * @param imChannelContext
	 */
	 void onSuccess(User user,ImChannelContext imChannelContext);

	/**
	 * 登陆失败回调方法
	 * @param imChannelContext
	 */
	 void onFailed(ImChannelContext imChannelContext);
}
