package org.jim.server.processor.chat;

import org.jim.core.ImChannelContext;
import org.jim.core.packets.RespBody;
import org.jim.server.command.handler.extend.pojo.ReCallReqBody;
import org.jim.server.processor.SingleProtocolCmdProcessor;

public interface ReCallCmdProcessor extends  SingleProtocolCmdProcessor{
	
	
	 RespBody reCall(ReCallReqBody body, ImChannelContext imChannelContext);
	
}
