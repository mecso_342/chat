package org.jim.server.command.handler.userInfo;

import org.jim.core.ImChannelContext;
import org.jim.core.config.ImConfig;
import org.jim.core.message.MessageHelper;
import org.jim.core.packets.User;
import org.jim.core.packets.UserReqBody;
import org.jim.core.packets.UserStatusType;
import org.jim.server.config.ImServerConfig;

import java.util.Objects;

/**
 * 持久化获取用户信息处理
 */
public class PersistentUserInfo implements IUserInfo {

    @Override
    public User getUserInfo(UserReqBody userReqBody, ImChannelContext imChannelContext) {
        ImServerConfig imServerConfig = ImConfig.Global.get();
        String userId = userReqBody.getUserId();
        Integer type = userReqBody.getType();
        //消息持久化助手;
        MessageHelper messageHelper = imServerConfig.getMessageHelper();
        User user = messageHelper.getUserByType(userId, type);
        boolean status = userReqBody.getOnlyUserStatus() == null ? false :userReqBody.getOnlyUserStatus();
        if(Objects.nonNull(user) && !status) {
        	user.setFriends(messageHelper.getAllFriendUsers(userId, UserStatusType.ALL.getNumber()));
            user.setGroups(messageHelper.getAllGroupUsers(userId, UserStatusType.ALL.getNumber()));
            //user.setFriends(messageHelper.getAllFriendUsers(userId, type));
            //user.setGroups(messageHelper.getAllGroupUsers(userId, type));
        }
        return user;
    }

}
