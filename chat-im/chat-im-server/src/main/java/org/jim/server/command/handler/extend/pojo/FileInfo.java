package org.jim.server.command.handler.extend.pojo;

import org.jim.core.http.UploadFile;
import org.jim.core.packets.Message;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户的文件信息
 * @author admin
 *
 */
public class FileInfo extends Message{
	
	private String from ;
	
	private String to;
	/**
	 * 消息类型;(如：0:text、1:image、2:voice、3:vedio)
	 */
	private Integer msgType;
	/**
	 * 聊天类型;(如公聊、私聊)
	 */
	private Integer chatType;
	
	
	private UploadFile file;


	public String getFrom() {
		return from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getTo() {
		return to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public Integer getMsgType() {
		return msgType;
	}


	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}


	public Integer getChatType() {
		return chatType;
	}


	public void setChatType(Integer chatType) {
		this.chatType = chatType;
	}


	public UploadFile getFile() {
		return file;
	}


	public void setFile(UploadFile file) {
		this.file = file;
	}
	
	
	
}
