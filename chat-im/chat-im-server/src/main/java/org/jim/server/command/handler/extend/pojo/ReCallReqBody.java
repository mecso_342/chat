package org.jim.server.command.handler.extend.pojo;

import org.jim.core.packets.Message;

import lombok.Getter;
import lombok.Setter;

/**
 * 撤回消息
 * @author admin
 *
 */
@Setter
@Getter
public class ReCallReqBody extends Message{
	
	/**
	 * 谁发的
	 */
	private String from;
	/**
	 * 发给谁
	 */
	private String to;
	/**
	 * @see ChatType  
	 * 1公聊  2私聊
	 */
	private Integer chatType;
	/**
	 * 消息ID
	 */
	private  String msgId;
	
}
