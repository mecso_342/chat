package org.jim.server.processor.group;

import org.jim.core.ImChannelContext;
import org.jim.core.packets.Group;
import org.jim.core.packets.JoinGroupRespBody;
import org.jim.core.packets.RespBody;
import org.jim.server.processor.SingleProtocolCmdProcessor;
/**
 * @author ensheng
 */
public interface GroupCmdProcessor extends SingleProtocolCmdProcessor {
    /**
     * 加入群组处理
     * @param joinGroup
     * @param imChannelContext
     * @return
     */
    JoinGroupRespBody join(Group joinGroup, ImChannelContext imChannelContext);
    
    /**
     * 创建组
     * @param groupId
     * @param imChannelContext
     * @return
     */
    RespBody addGroup(Group group, ImChannelContext imChannelContext);
    
    /**
     * 解散群
     * @param joinGroup
     * @param imChannelContext
     * @return
     */
    RespBody unbind(String groupId, ImChannelContext imChannelContext);
    
    /**
     * 退出群，群主不可以退。
     * 1.群主可以指定用户退出
     * 2.用户可以自己退出
     * @param joinGroup
     * @param imChannelContext
     * @return
     */
    RespBody removeUser(String groupId,String userId, Integer type, ImChannelContext imChannelContext);
    
    /**
     * 编辑组
     * @param groupId
     * @param imChannelContext
     * @return
     */
    RespBody editGroup(Group group, ImChannelContext imChannelContext);
    
    /**
     * 添加成员到群组
     * @param user
     * @param group
     * @param imChannelContext
     * @return
     */
    RespBody addUserToGroup(String groupId,String userIds, ImChannelContext imChannelContext);
    
    /**
     * 获取组信息
     * @param groupId
     * @param imChannelContext
     * @return
     */
    RespBody getGroup(String groupId, ImChannelContext imChannelContext);
    
}
