package org.jim.server.command.handler.extend;

import java.util.Objects;

import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.ImStatus;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.Group;
import org.jim.core.packets.RespBody;
import org.jim.core.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.processor.group.GroupCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 用户去添加群组
 *
 */
public class GroupInfoReqHandler extends AbstractCmdHandler {
	
	private static Logger log = LoggerFactory.getLogger(GroupInfoReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_INFO_GROUP_RESP,ImStatus.C2002);
		//获取群组
		Group group = JsonKit.toBean(packet.getBody(),Group.class);
		
		//如果有编辑的额外业务
		GroupCmdProcessor groupProcessor = this.getSingleProcessor(GroupCmdProcessor.class);
		if(Objects.nonNull(groupProcessor)){
			respBody = groupProcessor.getGroup(group.getGroupId(), imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (status) {
				respBody  = new RespBody(Command.COMMAND_INFO_GROUP_RESP, ImStatus.C2002);
				
			}
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			//respPacket.setStatus(ImStatus.C2002);
			return respPacket;
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_INFO_GROUP_REQ;
	}
}
