package org.jim.server.command.handler.extend.pojo;

import org.jim.core.packets.Message;
import org.jim.core.packets.MsgType;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户的文件信息
 * @author admin
 *
 */
@Getter
@Setter
public class FileInfoReqBody extends Message{
	
	

	private String fileName;
	
	private String fileUrl;
	
	private Long fileSize;
	
	
}
