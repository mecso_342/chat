package org.jim.server.processor;

import org.jim.core.ImChannelContext;
import org.jim.core.packets.RespBody;
import org.jim.core.packets.SystemNotify;
/**
 * 系统通知
 * @author admin
 *
 */
public interface SystemNotifyCmdProcessor extends SingleProtocolCmdProcessor {
    
	
    RespBody list(SystemNotify systemNotify, ImChannelContext imChannelContext);
    
    
    
}
