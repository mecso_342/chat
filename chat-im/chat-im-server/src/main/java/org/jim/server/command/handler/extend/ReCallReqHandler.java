package org.jim.server.command.handler.extend;

import java.util.Objects;

import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.ImStatus;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.core.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.extend.pojo.ReCallReqBody;
import org.jim.server.processor.chat.ReCallCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 用户去添加群组
 *
 */
public class ReCallReqHandler extends AbstractCmdHandler {
	
	private static Logger log = LoggerFactory.getLogger(ReCallReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_RECAL_RESP,ImStatus.C2002);
		//获取群组
		ReCallReqBody body = JsonKit.toBean(packet.getBody(),ReCallReqBody.class);
		
		//如果有编辑的额外业务
		ReCallCmdProcessor reCallCmdProcessor = this.getSingleProcessor(ReCallCmdProcessor.class);
		if(Objects.nonNull(reCallCmdProcessor)){
			respBody = reCallCmdProcessor.reCall(body, imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (status) {
				respBody  = new RespBody(Command.COMMAND_RECAL_RESP, ImStatus.C2002);
				
			}
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			//respPacket.setStatus(ImStatus.C2002);
			return respPacket;
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_RECAL_REQ;
	}
}
