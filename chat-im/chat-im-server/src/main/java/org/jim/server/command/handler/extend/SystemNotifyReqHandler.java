package org.jim.server.command.handler.extend;

import java.util.Objects;

import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.ImStatus;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.core.packets.SystemNotify;
import org.jim.core.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.processor.SystemNotifyCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 系统通知列表获取
 *
 */
public class SystemNotifyReqHandler extends AbstractCmdHandler {
	
	private static Logger log = LoggerFactory.getLogger(SystemNotifyReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_SYS_NOTIFY_RESP,ImStatus.C2002);
		//获取群组
		SystemNotify systemNotify = JsonKit.toBean(packet.getBody(),SystemNotify.class);
		
		//如果有编辑的额外业务
		SystemNotifyCmdProcessor systemNotifyCmdProcessor = this.getSingleProcessor(SystemNotifyCmdProcessor.class);
		if(Objects.nonNull(systemNotifyCmdProcessor)){
			respBody = systemNotifyCmdProcessor.list(systemNotify, imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (status) {
				respBody  = new RespBody(Command.COMMAND_SYS_NOTIFY_RESP, ImStatus.C2002);
			}
			ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
			//respPacket.setStatus(ImStatus.C2002);
			return respPacket;
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_SYS_NOTIFY_REQ;
	}
}
