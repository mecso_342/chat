package org.jim.server.command.handler.extend;

import java.util.Objects;

import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.ImStatus;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.RespBody;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.processor.FileCmdProcessor;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 上传文件
 *
 */
public class FileReqHandler extends AbstractCmdHandler {
	
	private static Logger logger = LoggerFactory.getLogger(FileReqHandler.class);
	
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext imChannelContext) throws ImException {
		RespBody respBody = new RespBody(Command.COMMAND_FILE_RESP,ImStatus.C2002);
		//ByteBuffer buffer = ByteBuffer.wrap(packet.getBody());
		//TODO 处理文件业务
		
		//如果有编辑的额外业务
		FileCmdProcessor groupProcessor = this.getSingleProcessor(FileCmdProcessor.class);
		if(Objects.nonNull(groupProcessor)){
			//respBody = groupProcessor.upload(fileInfo, imChannelContext);
			
			boolean status = Objects.isNull(respBody);
			if (!status) {
				ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
				return respPacket;				
			}
			
		}
		ImPacket respPacket = ProtocolManager.Converter.respPacket(respBody, imChannelContext);
		return respPacket;
	}
	@Override
	public Command command() {
		
		return Command.COMMAND_FILE_REQ;
	}
}
