##
该代码是基于作者J-IM提供的进行的二次补充和修改
##说明链接：
【腾讯文档】WS-chat请求补充业务说明
https://docs.qq.com/sheet/DRWFoWU51dUloZXhN

##这个项目需要知道的事情

1.业务系统和聊天系统是隔离的，采用MQ来维护数据的正确性
       具体代码再MqCousumeHandler中，我已注释。
       当新增用户或者删除用户时，同步消费用户的信息即可。具体实现可以参考。

2.上传文件
	因为我采用http方式、nginx进行代理。大家也可以不用nginx。需要再前端页面把IP地址写下
	我把用户的通道ImChannelContext 放入了内存里。直接取得的。
	没有采用socket传输  1.我比较菜，大文件传输可能改不好  2.功能需要着急上线，没时间做
	如果你的系统是集群的，需要改动它ChannelContextList 。订阅或者通知都可以。
	
	业务逻辑是：当绑定一个用户时在ImUserListener中put一个ImChannelContext
	
	当上传以后，先处理上传文件。然后根据userId获取到ImChannelContext
	（如果感觉不安全，你鉴权以后把token和userId一起绑定，让前端在header中传入token）
	然后发送给对方。自身获取响应。携带者文件名、大小、链接返回给用户
	
	
	2.还有就是你可以直接上传文件，返回文件信息，然后正常发送聊天
	
3.鉴权
	我采用的鉴权，把auth这部分代码改了改。因为我们的聊天是嵌入再系统里的，不需要登录。
	你需要再AuthServiceProcessor中把这个
	JSONObject json = http.post(url, js,JSONObject.class);
	改成自己的。或者弄个假数据就行.改好这里才能运行

4.关于channel还是要说一说
	当用户登录或者鉴权时，你也保不准这个人打开了几个浏览器，登录了或者鉴权了多少次。所以一个人是有多个通道的。
	所以再发送或者绑定组和人的时候特别要注意，比如userId绑定到群组，不要用通道去绑定，而是把userId所有通道都给绑定下。
	
	而且我发现用户只写了ws\http的协议的在线状态。但是一般我们都是ws协议。但是设备PC、andriod、ios也没写。
	这个后续也给补充下这个代码。
	

![输入图片说明](https://images.gitee.com/uploads/images/2021/1025/134342_f0b87588_1082215.png "微信图片_20211025134319.png")